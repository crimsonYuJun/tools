    <!-- footer -->
    <style>
        .list-inline-item a {
            color: #000;
            text-shadow: #3c3737 0px 0px 2px;
        }

        .list-inline-item a:hover {
            color: #fff;
            transition: all 0.5s !important;
            text-shadow: #007bff 1px 1px 1px;
        }
    </style>
    <footer class="footer bg-info">
        <div class="text-center container">
            <ul class="list-unstyled row justify-content-center">
                <li class="list-inline-item col-sm-2"><a href="backsys_index.php">工具借借後臺系統</a></li>
                <li class="list-inline-item col-sm-2"><a href="#" data-toggle="modal" data-target=".bd-example-modal-xl">
                        工具借借專題發表
                    </a></li>
                <li class="list-inline-item col-sm-2"><a href="turnjs4/index.html" target="_blank">我的平面作品</a></li>
                <li class="list-inline-item col-sm-2"><a href="Magazine/index.html" target="_blank">我的平面作品(flash)</a></li>
                <li class="list-inline-item col-sm-2"><a href="#" data-toggle="modal" data-target="#exampleModalCenter">
                        我的其他版面作品
                    </a></li>
            </ul>
        </div>
        <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5icLoMafFgo" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">我的其他版面設計</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ul>
                            <li><a href="https://crimsonsakura.github.io/brain-holes/" target="_blank">腦洞社團</a></li>
                            <li><a href="https://crimsonsakura.github.io/chk_list/" target="_blank">網路架設第三站確認清單</a></li>
                            <li><a href="https://crimsonsakura.github.io/candy/" target="_blank">Candy World</a></li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </footer>